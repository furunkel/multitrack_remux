LINK.cc = $(CXX) $(CXXFLAGS) $(ADDITIONAL_CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $(ADDITIONAL_LDFLAGS) $(TARGET_ARCH)

COMPILE.cc = $(CXX) $(CXXFLAGS) $(ADDITIONAL_CXXFLAGS) $(CPPFLAGS) $(TARGET_ARCH) -c

CXXFLAGS += -I/usr/local/include  # Directory where sndfile.h is installed
LIBS += -lsndfile
LFLAGS += 
EXECUTABLES = audio_combine audio_split

VPATH = .

all : $(EXECUTABLES)

audio_combine : audio_combine.o
	$(CXX) -o $@ $^ $(LFLAGS) $(ADDITIONAL_LFLAGS) $(LIBS) $(ADDITIONAL_LIBS) 
	strip audio_combine

audio_split : audio_split.o
	$(CXX) -o $@ $^ $(LFLAGS) $(ADDITIONAL_LFLAGS) $(LIBS) $(ADDITIONAL_LIBS) 
	strip audio_split

clean :
	rm -f *.o *.xSYM $(EXECUTABLES)
