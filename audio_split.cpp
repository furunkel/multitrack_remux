#include <sndfile.h>
#include <string.h>
#include <stdio.h>

#define MAX_NUM_OUTFILES     256
#define MAX_FRAMES_PER_CHUNK 64*1024
#define VERSION_STR          "1.00"

int main(int argc, char ** argv)
{   
   if (argc < 3)
   {
      printf("audio_split v%s:  Split a multichannel audio file into multiple mono files.\n", VERSION_STR);
      printf("Usage:  audio_split infile outfile1 [outfile2] [outfile3] [...]\n");
      return 1;
   }

   SF_INFO infileInfo; memset(&infileInfo, 0, sizeof(infileInfo));
   const char * infilename = argv[1];
   SNDFILE * infile = sf_open(infilename, SFM_READ, &infileInfo);
   if (infile == NULL)
   {
      printf("Error opening input file %s : %s\n", infilename, sf_strerror(NULL));
      return 1;
   };

   SNDFILE * outfiles[MAX_NUM_OUTFILES];
   const char * outfilenames[MAX_NUM_OUTFILES];
   int numOutputFiles = 0;
   for (int i=2; i<argc; i++)
   {
      if (numOutputFiles == infileInfo.channels)
      {
         printf("There were more output files specified than channels in the input file (%i), ignoring additional output file specifications.\n", infileInfo.channels);
         break;
      }

      SF_INFO nextInfo  = infileInfo;
      nextInfo.channels = 1;  // all output files will be mono
      outfilenames[numOutputFiles] = argv[i];
      outfiles[numOutputFiles]     = sf_open(outfilenames[numOutputFiles], SFM_WRITE, &nextInfo);
      if (outfiles[numOutputFiles]) 
      {
         printf("Opened output file [%s]\n", outfilenames[numOutputFiles]);

         // copy the metadata from the input file to each output file
         for (int k = SF_STR_FIRST; k <= SF_STR_LAST; k++)
         {
            const char * str = sf_get_string(infile, k);
            if (str != NULL) (void) sf_set_string(outfiles[numOutputFiles], k, str);
         }
         SF_INSTRUMENT inst; memset(&inst, 0, sizeof(inst));
         if (sf_command(infile, SFC_GET_INSTRUMENT, &inst, sizeof (inst)) == SF_TRUE) sf_command(outfiles[numOutputFiles], SFC_SET_INSTRUMENT, &inst, sizeof (inst));
      }
      else
      {
         printf("Error, unable to open input file [%s]\n", outfilenames[numOutputFiles]);
         return 1;
      }
      ++numOutputFiles;
   }
   if (numOutputFiles < infileInfo.channels) printf("WARNING:  Input file [%s] has %i channels, but only %i output files were specified.  The last %i streams will not be output.\n", infilename, infileInfo.channels, numOutputFiles, infileInfo.channels-numOutputFiles);
    
   printf("Splitting.... each of the %i mono output files will be %llu frames long\n", numOutputFiles, infileInfo.frames);

   // Now we do the actual audio combining.
   float * inBuf  = new float[MAX_FRAMES_PER_CHUNK*infileInfo.channels];
   float * outBuf = new float[MAX_FRAMES_PER_CHUNK];
   sf_count_t framesLeft = infileInfo.frames;
   while(framesLeft > 0)
   {
      sf_count_t chunkSizeFrames = (framesLeft < MAX_FRAMES_PER_CHUNK) ? framesLeft : MAX_FRAMES_PER_CHUNK;
      if (sf_readf_float(infile, inBuf, chunkSizeFrames) != chunkSizeFrames)
      {
         printf("Error, short read from input file [%s]\n", infilename);
         return 10;
      }

      for (int i=0; i<numOutputFiles; i++)
      {
         const float * inPtr = &inBuf[i];
         float * outPtr = outBuf;
         for (int j=0; j<chunkSizeFrames; j++) 
         {
            *outPtr++ = *inPtr;
            inPtr += infileInfo.channels;
         }   
         if (sf_writef_float(outfiles[i], outBuf, chunkSizeFrames) != chunkSizeFrames)
         {
            printf("Error, short write to output file [%s]\n", outfilenames[i]);
            return 10;
         }
      }
      framesLeft -= chunkSizeFrames;
   }
   delete [] inBuf;
   delete [] outBuf;

   for (int i=0; i<numOutputFiles; i++) sf_close(outfiles[i]);
   if (infile) sf_close(infile);

   printf("Audio split operation completed, exiting.\n");
   return 0;
}
