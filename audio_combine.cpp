#include <sndfile.h>
#include <string.h>
#include <stdio.h>

#define MAX_NUM_INFILES      256
#define MAX_FRAMES_PER_CHUNK 64*1024
#define VERSION_STR          "1.00"

int main(int argc, char ** argv)
{   
   if (argc < 3)
   {
      printf("audio_combine v%s:  Combine multiple audio files into a single multichannel file.\n", VERSION_STR);
      printf("Usage:  audio_combine outfile infile1 [infile2] [infile3] [...]\n");
      printf("Note that all input files should be the same length and sample rate.\n");
      printf("Audio channels will be ordered in the output file based on the order they\n");
      printf("were specified on the command line.\n");
      return 1;
   }

   SNDFILE * infiles[MAX_NUM_INFILES];
   const char * infilenames[MAX_NUM_INFILES];
   int infilechannelcounts[MAX_NUM_INFILES];
   int numInputFiles       = 0;
   int numOutputChannels   = 0;
   int maxNumInputChannels = 0;
   SF_INFO sfInfo;
   sf_count_t numInputFrames;

   for (int i=2; i<argc; i++)
   {
      if (numInputFiles == MAX_NUM_INFILES)
      {
         printf("Input file limit (%i) exceeed, ignoring additional input files.\n", MAX_NUM_INFILES);
         break;
      }

      SF_INFO nextInfo;
      infilenames[numInputFiles] = argv[i];
      infiles[numInputFiles]     = sf_open(infilenames[numInputFiles], SFM_READ, &nextInfo);
      if (infiles[numInputFiles]) printf("Opened input file [%s], which has %i channels and is %llu frames long...\n", infilenames[numInputFiles], nextInfo.channels, nextInfo.frames);
      else
      {
         printf("Error, unable to open input file [%s]\n", infilenames[numInputFiles]);
         return 1;
      }

      if (numInputFiles == 0) 
      {
         sfInfo = nextInfo;
         numInputFrames = sfInfo.frames;
      }
      else
      {
         if (nextInfo.samplerate != sfInfo.samplerate)
         {
            printf("Error, input file [%s] has a different sample rate (%i) than input file %s (%i)\n", infilenames[numInputFiles], nextInfo.samplerate, infilenames[0], sfInfo.samplerate);
            return 1;
         }
         if (nextInfo.frames != sfInfo.frames)
         {
            printf("WARNING: input file [%s] has a different length (%lli frames, expected %lli), output will be truncated to shortest stream!\n", infilenames[numInputFiles], nextInfo.frames, sfInfo.frames);
            if (numInputFrames > nextInfo.frames) numInputFrames = nextInfo.frames;
         }
      }
      numOutputChannels += nextInfo.channels;
      infilechannelcounts[numInputFiles] = nextInfo.channels;
      if (nextInfo.channels > maxNumInputChannels) maxNumInputChannels = nextInfo.channels;
      ++numInputFiles;
   }
    
   SF_INFO outfileInfo; memset(&outfileInfo, 0, sizeof(outfileInfo));
   outfileInfo.samplerate = sfInfo.samplerate;
   outfileInfo.channels   = numOutputChannels;
   outfileInfo.format     = sfInfo.format;

   const char * outfilename = argv[1];
   SNDFILE * outfile = sf_open(outfilename, SFM_WRITE, &outfileInfo);
   if (outfile == NULL)
   {
      printf("Error opening output file %s : %s\n", outfilename, sf_strerror(NULL));
      return 1;
   };

   // copy the metadata from the first input file to the output file
   for (int k = SF_STR_FIRST; k <= SF_STR_LAST; k++)
   {
      const char * str = sf_get_string(infiles[0], k);
      if (str != NULL) (void) sf_set_string(outfile, k, str);
   }
   SF_INSTRUMENT inst; memset(&inst, 0, sizeof(inst));
   if (sf_command(infiles[0], SFC_GET_INSTRUMENT, &inst, sizeof (inst)) == SF_TRUE) sf_command(outfile, SFC_SET_INSTRUMENT, &inst, sizeof (inst));

   printf("Combining.... output file [%s] will be %llu frames long and will contain %i audio channels from %i input files\n", outfilename, sfInfo.frames, numOutputChannels, numInputFiles);

   // Now we do the actual audio combining.
   float * inBuf  = new float[MAX_FRAMES_PER_CHUNK*maxNumInputChannels];
   float * outBuf = new float[MAX_FRAMES_PER_CHUNK*numOutputChannels];
   sf_count_t framesLeft = numInputFrames;
   while(framesLeft > 0)
   {
      sf_count_t chunkSizeFrames = (framesLeft < MAX_FRAMES_PER_CHUNK) ? framesLeft : MAX_FRAMES_PER_CHUNK;
      int curOutputChannel = 0;
      for (int i=0; i<numInputFiles; i++)
      {
         if (sf_readf_float(infiles[i], inBuf, chunkSizeFrames) != chunkSizeFrames)
         {
            printf("Error, short read from input file [%s]\n", infilenames[i]);
            return 10;
         }
         const float * inPtr = inBuf;
         float * outPtr = &outBuf[curOutputChannel];
         for (int j=0; j<chunkSizeFrames; j++) 
         {
            memcpy(outPtr, inPtr, infilechannelcounts[i]*sizeof(float));  // copy the data over
            inPtr  += infilechannelcounts[i];                             // advance the input one frame
            outPtr += numOutputChannels;                                  // advance the output one frame
         }   
         curOutputChannel += infilechannelcounts[i];
      }
      if (sf_writef_float(outfile, outBuf, chunkSizeFrames) != chunkSizeFrames)
      {
         printf("Error, short write to output file [%s]\n", outfilename);
         return 10;
      }
      framesLeft -= chunkSizeFrames;
   }
   delete [] inBuf;
   delete [] outBuf;

   for (int i=0; i<numInputFiles; i++) sf_close(infiles[i]);
   if (outfile) sf_close(outfile);

   printf("Audio combine operation completed, exiting.\n");
   return 0;
}
