require 'date'

def sh(cmd)
  puts cmd
  `#{cmd}`
end

input_filename = ARGV[0]
output_filename = ARGV[1]
p ARGV
audio_files = ARGV[2..4] #["01.wav", "02.wav", "03.wav"]
audio_stream_files = audio_files.repeated_permutation(2).map do |perm|
  filename = "/tmp/#{perm.join('_').gsub('.','_').gsub(/\s/, '_')}.wav"

  sh "#{__dir__}/audio_combine '#{filename}' #{perm.map{|p| "'#{p}'"}.join ' '}"
  filename
end

is = audio_stream_files.map{|a| "-i '#{a}'"}.join(' ')
acodec = Array.new(audio_stream_files.size - 1, "-acodec libfaac -newaudio").join(' ')
imaps = Array.new(audio_stream_files.size){|i| "-map #{i+1}:#{0}"}.join(' ')
omaps = Array.new(audio_stream_files.size){|i| "-c:a:#{i} libmp3lame -b:a:#{i} 128k"}.join(' ')
                                          
sh "ffmpeg -i '#{input_filename}' #{is} -map 0:0  #{imaps}  -c:v copy #{omaps} '#{output_filename}'"
sh "ffmpeg -i '#{output_filename}' -r 1 -vframes 1 -ss 00:00:00 '#{output_filename[/.*(?=\..+$)/]}.jpg'"
