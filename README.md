# multitrack_remix

Remuxes a video file with all possible combinations of a given set of audio tracks.

## Requirements
 * ruby
 * make
 * git

## Installation

Clone this repository

    $ git clone https://furunkel@bitbucket.org/furunkel/multitrack_remux.git  

Then change into the `multitrack_remux` directory and type

    $ make

to build the `audio_combine` and `audio_split` executables.


## Usage

Run

    $ ruby multitrack_remux.rb <input filename> <output filename>

in the project directory.

**Note:** The audio tracks must have names `01.wav`, `02.wav` and `03.wav` and must be raw wave files.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
